import {Component, ViewChild, ElementRef} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MzButtonModule, MzInputModule, MzModalModule,MzNavbarModule,MzModalService,MzCollectionModule } from 'ngx-materialize';
import { ModalComponent } from './modal/modal.component';
import { ModalErrorComponent } from './modal-error/modal-error.component';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
  title = 'front';
   
}
