import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { MzButtonModule, MzInputModule, MzModalModule,MzNavbarModule,MzModalService,MzCollectionModule } from 'ngx-materialize';
import { ModalComponent } from 'src/app/modal/modal.component';
import { ModalErrorComponent } from 'src/app/modal-error/modal-error.component';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  form = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    option: new FormControl('1', Validators.required)
  });
  constructor(private http: HttpClient,private modalService: MzModalService) { }

  get f(){
    return this.form.controls;
  }
  
  public openServiceModal(modal) {
    this.modalService.open(modal);
  }

  ngOnInit() {
  }

  submit(){
    console.log(this.form.value);
    this.http.post<any>('http://localhost:8080/survey/create', { email: this.form.get('email').value, option: this.form.get('option').value }).subscribe({
        next: data => {
          console.log('ok');
          this.openServiceModal(ModalComponent);
          
        },
        error: error => {
            console.error('Hubo un error', error);
            this.openServiceModal(ModalErrorComponent);
        }
    })
    
  }

}
