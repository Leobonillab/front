import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MzButtonModule, MzInputModule,MzTabModule,MzModalModule,MzNavbarModule, MzCollectionModule,MzBadgeModule,MzIconModule, MzIconMdiModule  } from 'ngx-materialize';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UniqueEmailValidatorDirective } from './shared/unique-email-validator.directive';
import { ModalComponent } from './modal/modal.component';
import { ModalErrorComponent } from './modal-error/modal-error.component';
import { AnswersComponent } from './answers/answers.component';
import { OptionPipe } from './pipes/option.pipe';
import { FormComponent } from './form/form.component';

@NgModule({
  declarations: [
    AppComponent,
    UniqueEmailValidatorDirective,
    ModalComponent,
    ModalErrorComponent,
    AnswersComponent,
    OptionPipe,
    FormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MzButtonModule, 
    MzInputModule,
    MzModalModule,
    MzNavbarModule,
    MzCollectionModule,
    MzBadgeModule,
    MzIconModule, 
    MzIconMdiModule,
    MzTabModule
    
  ],
  providers: [OptionPipe],
  bootstrap: [AppComponent],
  entryComponents:[  ModalComponent, ModalErrorComponent  ]
})
export class AppModule { }
