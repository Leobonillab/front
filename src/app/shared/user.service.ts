import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = 'http://localhost:8080/email';

  constructor(private http: HttpClient ) { 
  }

  getUserByEmail(email: string){
  		return this.http.get<any[]>(`${this.url}/${email}`);
  }
}
