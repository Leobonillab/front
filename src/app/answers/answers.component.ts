import { Component, OnInit } from '@angular/core';
import { SurveyService } from 'src/app/shared/survey.service';
import { MzCollectionModule,MzBadgeModule, MzTabModule } from 'ngx-materialize'
import {HttpClientModule, HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss']
})
export class AnswersComponent implements OnInit {
  private intervalUpdate: any = null;
  public chart: any = null;
  constructor(private surveyService: SurveyService, private http: HttpClient) { }
  answers;
  ngOnInit() {
    
    this.surveyService.getCount().subscribe((count) => {
        
        this.chart = new Chart('pieChart', {
          type: 'pie',
          data: {
            labels : [
              'No toma', 'Light', 'Sin Azucar', 'Normal'
            ],
            datasets :[{
              label: 'Bebidas',
              data:[count[0],count[1], count[2], count[3]],
              backgroundColor:[
                'red', 'blue', 'green','yellow'
              ]
            }]
    
          }
    
        });

    });
    
    this.surveyService.getAll().subscribe((data)=>{
      console.log(data);
      this.answers = data;
    });
  }

  private ngOnDestroy(): void {
   }

  getAll(){

  }

  /**
 * Print the data to the chart
 * @function showData
 * @return {void}
 */
 private showData(): void {
  
}
 

private getFromAPI(){
  return this.surveyService.getCount();
}

}
